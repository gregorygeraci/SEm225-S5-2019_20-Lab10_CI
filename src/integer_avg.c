//
// Created by gerac on 06.01.2020.
//
#include "integer_avg.h"

/* Function that return the sum of two integers */
int integer_avg(int a, int b)
{
    return (a + b)/2;
}

