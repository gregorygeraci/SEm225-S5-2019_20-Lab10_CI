//
// Created by gerac on 06.01.2020.
//
#include "integer_avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* set stuff up here */
}

void tearDown(void)
{
    /* clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(2, integer_avg(3, 1), "Error in integer_sum");
    //TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(3, 1), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-4, integer_avg(-10, 2), "Error in integer_sum");
}
